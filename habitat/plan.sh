pkg_name="dummy-service"
pkg_origin="atrniv"
pkg_version="2.1"
pkg_deps=(core/bash-static)
svc_user="hab"
svc_group="hab"

# Habitat provides you with a number of built-in "callbacks" to use
# in the course of your build, all of which are explained in the docs
# at https://habitat.sh/docs/reference/#reference-callbacks.
#
# Here, we're implementing the do_build and do_install callbacks
# to install dependencies and assemble the application package.

do_build() {
  return 0
}

do_install() {

  return 0
}
